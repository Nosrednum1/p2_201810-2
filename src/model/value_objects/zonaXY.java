package model.value_objects;

import model.data_structures.Queue;
import model.data_structures.RedBlackBST;

public class zonaXY implements Comparable<zonaXY>{

	private RedBlackBST<Servicio> servicios;

	private String key;

	public zonaXY(String key) {
		if(key == null)throw new IllegalArgumentException("No puedo crear una zonaXY sin valor de llave");
		servicios = new RedBlackBST<>();
		this.key = key;
	}

	public void insertService(Servicio srvc) {servicios.put(srvc);}

	public String getKey() {return key;}

	public void getServices(Queue<Servicio> services) {
		for (Servicio servicio : servicios.keys()) {
			services.add(servicio);
		}
	}

	@Override
	public int compareTo(zonaXY arg0) { if(arg0 == null)throw new IllegalArgumentException("No puedo compararte algo inexistente");
	return this.key.compareTo(arg0.key);}

}
