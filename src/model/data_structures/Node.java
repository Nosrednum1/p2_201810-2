package model.data_structures;

public class Node <T extends Comparable<T>>{

	private static final boolean RED = true;
	private static final boolean BLACK = false;

	private boolean color;
	private T item;
	private int size;
	private Node<T> left, right;

	public Node(T value) {
		size = 1; left = right = null; color = RED;}

	public Node<T> left() {return left;}
	public Node<T> right() {return right;}
	public boolean color() {return color;}
	public T item() {return item;}

	public void add(Node<T> n) {
		if(n.item().compareTo(this.item)<0) {
			if(left != null) left.add(n);
			else left = n;
			return;}
		if(right == null)right = n;
		right.add(n);
	}
	public int size() {return size;}
	public void changeColor() {this.color = BLACK;}

	public void a(T key) {}

}
