package model.data_structures;

public class LinearProbingHT <Key extends Comparable<Key>, Value extends Comparable<Value>>{

	private static final int MINIMUM_CANT = 10;

	private int size, spacesSize;

	private Key[] keys;
	private Value[] values;

	public LinearProbingHT() {this(MINIMUM_CANT);}
	public LinearProbingHT(int capacity) {
		size = 0; spacesSize = capacity;
		keys = (Key[]) new Object[spacesSize];
		values = (Value[]) new Object[spacesSize];
	}

	public int size() {return size;} public boolean isEmpty() {return size == 0;}

	private void resize(int capacity) {
		LinearProbingHT<Key, Value> temp = new LinearProbingHT<Key, Value>(capacity);
		for (int i = 0; i < spacesSize; i++) 
			if (keys[i] != null) temp.put(keys[i], values[i]);
		keys = temp.keys;	values = temp.values;	spacesSize = temp.spacesSize;
	}

	public void put(Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("la llave del valor es null");
		if (val == null) {	delete(key);	 return;}

		if (size >= spacesSize/2) resize(2*spacesSize);

		int i;
		for (i = hash(key); keys[i] != null; i = (i + 1) % spacesSize) {
			if (keys[i].equals(key)) {
				values[i] = val;
				return;
			}
		}
		keys[i] = key;
		values[i] = val;
		size++;
	}

	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("No puedo devolverte un elemento con key null");
		for (int i = hash(key); keys[i] != null; i = (i + 1) % spacesSize)
			if (keys[i].equals(key))
				return values[i];
		return null;}

	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("No puedo devolverte un elemento con key null");
		return get(key) != null;}

	public void delete(Key key) {
		if (key == null) throw new IllegalArgumentException("No puedo eliminar algo null");
		if (!contains(key)) return;

		// find position i of key
		int i = hash(key);
		while (!key.equals(keys[i])) i = (i + 1) % spacesSize;

		// delete key and associated value
		keys[i] = null;
		values[i] = null;

		// rehash all keys in same cluster
		i = (i + 1) % spacesSize;
		while (keys[i] != null) {
			// delete keys[i] an vals[i] and reinsert
			Key   keyToRehash = keys[i];
			Value valToRehash = values[i];
			keys[i] = null;
			values[i] = null;
			size--;
			put(keyToRehash, valToRehash);
			i = (i + 1) % spacesSize;
		}
		size--;

		// halves size of array if it's 12.5% full or less
		if (size > 0 && size <= spacesSize/8) resize(spacesSize/2);

		assert check();
	}

	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < spacesSize; i++)
			if (keys[i] != null) queue.add(keys[i]);
		return queue;
	}

	private boolean check() {
		// revisa la plenitud minima del 50% de la tabla
		if (spacesSize < 2*size) {
			System.err.println("Hash table size m = " + spacesSize + "; array size n = " + size);
			return false;
		}
		// revisa que cada llave pueda ser encontrada
		for (int i = 0; i < spacesSize; i++) {
			if (keys[i] == null) continue;
			else if (get(keys[i]) != values[i]) {
				System.err.println("get[" + keys[i] + "] = " + get(keys[i]) + "; vals[i] = " + values[i]);
				return false;}}
		return true;
	}

	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % spacesSize;
	}

}
