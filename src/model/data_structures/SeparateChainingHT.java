package model.data_structures;

/**
 * @author Anderson Barrag�n Agudelo
 */
public class SeparateChainingHT <Key extends Comparable<Key>, Value extends Comparable<Value>>{

	private static final int CAPACIDAD_INICIAL = 10;

	private int elementsSize, tableSize; //el primero, es la cantidad de elementos, el segundo el tama�o de la tabla
	private SequentialSearchST<Key, Value>[] data;


	public SeparateChainingHT() {
		this(CAPACIDAD_INICIAL);
	} 

	public SeparateChainingHT(int tableSize) {
		this.tableSize = tableSize;
		data = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[tableSize];
		for (int i = 0; i < tableSize; i++)
			data[i] = new SequentialSearchST<Key, Value>();
	} 

	// resize the hash table to have the given number of chains,
	// rehashing all of the keys
	private void resize(int chains) {
		SeparateChainingHT<Key, Value> temp = new SeparateChainingHT<Key, Value>(chains);
		for (int i = 0; i < tableSize; i++) {
			for (Key key : data[i].keys()) {
				temp.put(key, data[i].get(key));
			}
		}
		this.tableSize  = temp.tableSize;
		this.elementsSize  = temp.elementsSize;
		this.data = temp.data;
	}

	// hash value between 0 and m-1
	private int hash(Key key) {
		return (key.hashCode() & 0x7fffffff) % tableSize;
	} 

	/**
	 * Returns the number of key-value pairs in this symbol table.
	 *
	 * @return the number of key-value pairs in this symbol table
	 */
	public int size() {
		return elementsSize;
	} 

	/**
	 * Returns true if this symbol table is empty.
	 *
	 * @return {@code true} if this symbol table is empty;
	 *         {@code false} otherwise
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Returns true if this symbol table contains the specified key.
	 *
	 * @param  key the key
	 * @return {@code true} if this symbol table contains {@code key};
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public boolean contains(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to contains() is null");
		return get(key) != null;
	} 

	/**
	 * Returns the value associated with the specified key in this symbol table.
	 *
	 * @param  key the key
	 * @return the value associated with {@code key} in the symbol table;
	 *         {@code null} if no such value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public Value get(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		int i = hash(key);
		return data[i].get(key);
	} 

	/**
	 * Inserts the specified key-value pair into the symbol table, overwriting the old 
	 * value with the new value if the symbol table already contains the specified key.
	 * Deletes the specified key (and its associated value) from this symbol table
	 * if the specified value is {@code null}.
	 *
	 * @param  key the key
	 * @param  val the value
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void put(Key key, Value val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}

		// double table size if average length of list >= 10
		if (elementsSize >= 10*tableSize) resize(2*tableSize);

		int i = hash(key);
		if (!data[i].contains(key)) elementsSize++;
		data[i].put(key, val);
	} 

	/**
	 * Removes the specified key and its associated value from this symbol table     
	 * (if the key is in this symbol table).    
	 *
	 * @param  key the key
	 * @throws IllegalArgumentException if {@code key} is {@code null}
	 */
	public void delete(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");

		int i = hash(key);
		if (data[i].contains(key)) elementsSize--;
		data[i].delete(key);

		// halve table size if average length of list <= 2
		if (tableSize > CAPACIDAD_INICIAL && elementsSize <= 2*tableSize) resize(tableSize/2);
	} 

	// return keys in symbol table as an Iterable
	public Iterable<Key> keys() {
		Queue<Key> queue = new Queue<Key>();
		for (int i = 0; i < tableSize; i++) {
			for (Key key : data[i].keys())
				queue.add(key);
		}
		return queue;
	} 
}
