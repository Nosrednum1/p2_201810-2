package viewer;

import java.util.Scanner;

import model.data_structures.IList;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.logic.Project2Manager;
import model.value_objects.Servicio;
import model.value_objects.TaxiConPuntos;
import model.value_objects.TaxiConServicios;

public class Viewer {
	private static final Timer t = new Timer();
	private static final Project2Manager manager = new Project2Manager();

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		while(!fin) {	printMenu();			
		int opcion = sc.nextInt();
		switch (opcion) {
		case 1:	
			printMenuLoad();
			int optionCargar = sc.nextInt();
			//
			String linkJson = "";
			switch (optionCargar){
			case 1:	linkJson = manager.DIRECCION_SMALL_JSON;	break; //Direcci�n del archivo de datos peque�o
			case 2:	linkJson = manager.DIRECCION_MEDIUM_JSON;	break; //Direcci�n del archivo de datos mediano
			case 3:	linkJson = manager.DIRECCION_LARGE_JSON;	break; //Direcci�n del archivo de datos grande
			case 4:	linkJson = manager.DIRECCION_TEST_JSON;		break;} // Test con pocos taxis pero varios casos
			System.out.println("Datos cargados: " + linkJson);
			t.start();
			manager.load(linkJson);
			t.end();	break;
		case 2:	//1A

			//id zona
			System.out.println("Ingrese el id de la zona");	String zonaIDReq1A = sc.next();
			int idZona = 0;
			try  { idZona = Integer.parseInt(zonaIDReq1A); }
			catch (Exception e) { System.err.println("id invalido"); break; }

			//nombre compania
			System.out.println("Ingrese el nombre de la compania");	sc.nextLine();
			String companiaReq1A = sc.nextLine();

			//Req 1A
			// Mostrar el Id del (de los) taxi(s) y fecha/hora de inicio de sus servicios iniciando en la zona dada
			IList<TaxiConServicios> listaTaxis = manager.A1TaxiConMasServiciosEnZonaParaCompania(idZona, companiaReq1A);
			for(TaxiConServicios taxi: listaTaxis){
				taxi.print();
			}
			break;

		case 3://2A

			//Duracion de la consulta
			System.out.println("Ingrese la duracion de consulta (segundos)");
			String duracionS = sc.next();
			int duracion = 0;
			try {
				duracion = Integer.parseInt(duracionS);
			} catch (Exception e) {
				System.err.println("Duracion invalida");
				break;
			}

			// Req 2A
			IList<Servicio> listaServicios = manager.A2ServiciosPorDuracion(duracion);
			for(Servicio s : listaServicios){
				System.out.println("IdTaxi: " + s.getTaxiId()
				+ "  IdServicio: "+ s.getTripId()
				+ "  Duracion: "+ s.getTripSeconds());
			}
			break;

			//_______________________________________________________
		case 4: //1B
			double distanciaMinima = 0; //Distancia minima
			System.out.println("Ingrese la distancia minima (millas)");	String distMinReq1B = sc.next();

			double distanciaMaxima = 0;	//Distancia maxima
			System.out.println("Ingrese la distancia maxima (millas)"); String distMaxReq1B= sc.next();

			try {
				distanciaMinima = Double.parseDouble(distMinReq1B);
				distanciaMaxima = Double.parseDouble(distMaxReq1B);
			} catch (Exception e) {	System.err.println("Distancia invalida");	break;}

			if ( distanciaMinima > distanciaMaxima ) {System.err.println("Rango de distancias invalido");break;}

			// Req 1B
			Queue<Servicio> listaServicios2 = manager.B1ServiciosPorDistancia(distanciaMinima, distanciaMaxima);
			int cise = listaServicios2.size();
			for(Servicio s : listaServicios2)
				System.out.println("IdTaxi: " + s.getTaxiId()
				+ "  IdServicio: "+ s.getTripId()
				+ "  Distancia: "+ s.getTripMiles());
			System.out.println(cise);
			break;

		case 5: //2B
			int idZonaRecogida = 0;   //idZonaRecogida
			System.out.println("Ingrese el id de la zona de recogida"); String idZonaRecogidaReq2B = sc.next();

			int idZonaTerminacion = 0;  //idZonaTerminacion
			System.out.println("Ingrese el id de la zona de terminacion"); String idZonaTerminacionReq2B = sc.next();
			try {
				idZonaRecogida = Integer.parseInt(idZonaRecogidaReq2B);
				idZonaTerminacion = Integer.parseInt(idZonaTerminacionReq2B);
			}
			catch (Exception e) {System.err.println("id invalido");	break;}

			System.out.println("Ingrese la fecha inicial (Ej : 2017-02-01)");
			String fechaInicialReq2B = sc.next();	//Fecha Inicial

			System.out.println("Ingrese la hora inicial (Ej: 09:00:00.000)");
			String horaInicialReq2B = sc.next();	//Hora inicial

			System.out.println("Ingrese la fecha final (Ej : 2017-02-01)");
			String fechaFinalReq2B = sc.next();	//fecha final

			System.out.println("Ingrese la hora final (Ej: 09:00:00.000)");
			String horaFinalReq2B = sc.next();	//hora final

			// Req 2B
			IList<model.value_objects.Servicio> listaServicios3 =
					manager.B2ServiciosPorZonaRecogidaYLlegada(idZonaRecogida, idZonaTerminacion, fechaInicialReq2B, fechaFinalReq2B, horaInicialReq2B, horaFinalReq2B);
			for(Servicio s : listaServicios3){
				System.out.println("Id Trip: "+ s.getTripId());
				System.out.println("  Zona recogida: " + s.getPickArea());
				System.out.println("  Zona terminacion: " + s.getDropArea());
				System.out.println("  Hora inicial: " + s.getTripStart());
			}

			break;

		case 6: //1C

			TaxiConPuntos[] taxis = manager.R1C_OrdenarTaxisPorPuntos();
			for (TaxiConPuntos t : taxis) if(t != null) 
				System.out.println("el taxi con identificador: "+t.taxiId()+ "\n"+ "     " + "tuvo un puntaje de: "+t.getPuntos());

			//   Si el conjunto ordenado tiene menos de 10 taxis, hay que mostrar todos taxis en orden ascendente de puntos. 
			//   Por cada taxi mostrar su Id y sus puntos
			//   Si el conjunto ordenado tiene 10 taxis o mas, hay que mostrar los 5 primeros taxis y los 5 ultimos taxis resultado del ordenamiento. 
			//   Por cada taxi mostrar su Id y sus puntos "\u001B[31m"
			break;

		case 7: //2C

			System.out.println("Ingrese el id del taxi");
			String taxiIDReq2C = sc.next();

			System.out.println("Ingrese la cantidad X de millas");
			double millas=0;
			String millasReq2C = sc.next();
			try{ millas = Double.parseDouble(millasReq2C); }
			catch(Exception e){	System.out.println("Cantidad invalida"); }

			// Req 2C
			Queue<Servicio> servicios2C = manager.R2C_LocalizacionesGeograficas(taxiIDReq2C, millas);

			for(Servicio s : servicios2C)
			{
				System.out.println();
				System.out.println("Servicio: " + s.getTripId());
				//TODO imprimir la latitud y la longitud de los servicios
				System.out.println("  (Lat: " + s.getPickupLatitud() + ", Long: " + s.getPickupLongitud() + " )");
				System.out.println("  Distancia (millas) a la referencia: " + "�<calcular>?");
			}

			break;

		case 8: //3C
			System.out.println("Ingrese la fecha (Ej : 2017-02-01)"); //fecha 
			String fecha = sc.next();

			//hora 
			System.out.println("Ingrese la hora de inicio con minutos arbitrarios en rango [0, 59] (Ej: 09:55:00.000)");
			String hora = sc.next();

			// Req 3C
			Queue<Servicio> servicios3C = manager.R3C_ServiciosEn15Minutos(fecha, hora);

			for(Servicio s : servicios3C){
				System.out.println();
				System.out.println("Servicio: " + s.getTripId());
				System.out.println("  Taxi: " + s.getTaxiId());
				System.out.println("  Zona recogida: " + s.getPickArea());
				System.out.println("  Zona terminacion: " + s.getDropArea());
				System.out.println("  Fecha/Hora inicial: " + s.getTripStart());
			}	break;
		case 9:	 fin = true; sc.close(); break;
		}}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");

		System.out.println("\nParte A:\n");
		System.out.println("2. Obtener el taxi con mas servicios en una zona dada para una determinada compania (1A)");
		System.out.println("3. Obtener los servicios con duracion en el minuto al que corresponde una duracion de consulta (en segundos) (2A)");

		System.out.println("\nParte B:\n");
		System.out.println("4. Obtener los servicios cuya distancia recorrida esta en un rango de distancia en millas (1B)");
		System.out.println("5. Obtener los servicios que iniciaron en una zona de recogida y terminaron en una zona de terminacion entre una fecha/hora inicial y una fecha/Hora final (2B)");


		System.out.println("\nParte C:\n");
		System.out.println("6. Ordenamiento de taxis utilizando un sistema de puntos (1C)");
		System.out.println("7. Informar las localizaciones geograficas (latitud,longitud) de los servicios iniciados por un taxi que estan dentro de la zona a X millas de una localizacion geografica (latitud, longitud) dada (2C)");
		System.out.println("8. Obtener los servicios que inician en el rango de 15 minutos mas cercano a una fecha/hora dadas (con minutos arbitrarios en rango [0, 59]), saliendo de una zona y terminado en otra zona (3C)");
		System.out.println("9. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}
	private static void printMenuLoad() {
		System.out.println("-- �Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- 4. Test");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
}
